#!/usr/bin/env bash
set -euo pipefail

checkGitCryptAvailability () {
  if ! type git-crypt > /dev/null; then
      printf "\ngit-crypt command is not available. Install git-crypt\n";
      exit 1;
  fi
}

checkUnlockKeysAvailability () {
  if ! [[ $(ls -A ../keys) ]]; then
      printf "\nkeys directory is empty. Make sure it contains repository unlock key files\n";
      exit 1;
  fi
}

unlockRepository () {
  sh -c "cd ../area-51-dev && git-crypt unlock ../keys/area-51-dev.key"
}

mkdir -p ../keys
checkGitCryptAvailability
checkUnlockKeysAvailability
unlockRepository
