#!/usr/bin/env bash
set -euo pipefail

cloneRepos () {
  repos=("area-51-dev" "iac-server" "hydra-webapi" "hydra-webfe" "hydra-booking")

  for repo in "${repos[@]}"; do
      printf "\n"
      git clone git@gitlab.com:beast-makers/${repo}.git ../${repo}
  done
}

cloneRepos
