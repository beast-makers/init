#!/usr/bin/env bash

BEASTMAKERS_REPO_PRIVATE_KEY_FILE=~/.ssh/id_rsa_beast_makers

generateSshKeyQuestion () {
    read -p 'Do you want to overwrite the ssh key with a new one ? [y/n] ' yn
    if [ $yn = 'y' ]; then
        runSshKeyGenerationCommands
    else
        printf "\tSkipping ssh key generation\n\n"
    fi
}

runSshKeyGenerationCommands () {
    generateSshKey
    createSshConfigEntry
    printf "Add it to your git account or send it to your supervisor\n"
    printf -- "--- BEGIN PUBLIC KEY ---\n"
    cat "${BEASTMAKERS_REPO_PRIVATE_KEY_FILE}.pub"
    printf -- "--- END PUBLIC KEY---\n"
}

generateSshKey () {
    nextStep=false
    until $nextStep; do
        read -p 'Give me your email: ' userEmail
        read -p "Is this correct? [y/n] '${userEmail}' " yn
        case $yn in
            [Yy]* )
                nextStep=true
                ;;
            [Nn]* )
                ;;
            * ) printf "\tPlease answer (y) or (n).\n";;
        esac
    done

    ssh-keygen -o -t rsa -b 4096 -C "${userEmail} beast-makers repos" -f ${BEASTMAKERS_REPO_PRIVATE_KEY_FILE}
}

createSshConfigEntry () {
    if [[ ! -f ~/.ssh/config ]]; then
       touch ~/.ssh/config
    fi

    if ! grep -q -- '-- beast-makers_repos' ~/.ssh/config; then
    tee -a ~/.ssh/config << CONFIG
# GitLab.com -- beast-makers_repos
Host gitlab.com
  Preferredauthentications publickey
  IdentityFile ${BEASTMAKERS_REPO_PRIVATE_KEY_FILE}
CONFIG
fi
}

if [[ -f BEASTMAKERS_REPO_PRIVATE_KEY_FILE ]]; then
    generateSshKeyQuestion
else
    runSshKeyGenerationCommands
fi
