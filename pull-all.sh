#!/usr/bin/env bash
set -euo pipefail

pullFromAllRepos () {
  repos=("area-51-dev" "iac-server" "hydra-webapi" "hydra-webfe" "hydra-booking")

  for repo in "${repos[@]}"; do
      printf "\nPulling from [${repo}]\n"
      sh -c "cd ../${repo} && git pull --rebase"
  done
}

pullFromAllRepos
